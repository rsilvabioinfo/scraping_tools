import json
import requests
import xmltodict
import re

with open('metlin_struct.json') as f:
	struct = json.load(f)

print(len(struct))

struct2 = {}
for k,v in struct.items():
    tmp = {}
    [tmp.update(x) for x in v]
    struct2[k] = tmp

df = pd.DataFrame(struct2)
df = df.T
df.to_csv('metlin_dataframe.tsv', sep='\t', index=None)

def get_pubchem(cid_list):
    cid = ','.join(cid_list)
    url = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/%s/property/InChI/JSON' % cid
    txt = requests.get(url).text
    return json.loads(txt)['PropertyTable']['Properties']

def sid2cid(sid_list):
    sid = ','.join(sid_list)
    url = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/substance/sid/%s/cids/JSON' % sid
    txt = requests.get(url).text
    return json.loads(txt)["InformationList"]["Information"]

def hmdb2inchi(hmdb):
    hmdb = 'HMDB{0:07d}'.format(int(re.sub('\D', '', hmdb)))
    url =  'https://hmdb.ca/metabolites/%s.xml' % hmdb
    txt = requests.get(url).text
    xml = xmltodict.parse(txt)
    return xml['metabolite']['inchi']

def cas2cid(cas_number):
    url = 'https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/name/%s/cids/JSON' % cas_number
    #https://cactus.nci.nih.gov/chemical/structure/15895-41-7/smiles
    #https://www.ncbi.nlm.nih.gov/entrez/eutils/esearch.fcgi?db=pccompound&retmax=100&term=
    resp = requests.get(url)
    if resp.status_code!=200:
        return None
    txt = resp.text
    return json.loads(txt)["IdentifierList"]["CID"][0]

kegg2pubchem = pd.read_csv('http://rest.kegg.jp/conv/cpd/pubchem', header=None,
                           sep='\t')
kegg2pubchem[0] = kegg2pubchem[0].str.replace('pubchem:', '')
kegg2pubchem[1] = kegg2pubchem[1].str.replace('cpd:', '')
kegg2pubchem =  kegg2pubchem[kegg2pubchem[1].isin(df['KEGG'])]
kegg2pubchem.reset_index(inplace=True, drop=True)

maxrange = len(kegg2pubchem)
chunksize = 100
kegglist = []
for i in range(0, maxrange, chunksize):
   to_query = kegg2pubchem.loc[i:(i+chunksize), 0].tolist()
   kegglist.extend(sid2cid(to_query))

kegglist_updated = []

for x in kegglist:
    if 'CID' in x.keys():
        if type(x['CID'])==list:
            x['CID'] = x['CID'][0]
        kegglist_updated.append(x)

kegg2pubchem[0] = kegg2pubchem[0].astype(int)
kegg2pubchem = pd.merge(kegg2pubchem, pd.DataFrame(kegglist_updated),
                        left_on=0, right_on='SID')

cids = df.loc[(df['PubChem']!='') & (df['PubChem']!='0'), 'PubChem'].tolist()
cids.extend(kegg2pubchem['CID'].astype(str))
cids = pd.unique(cids).tolist()
maxrange = len(cids)
chunksize = 100
cidlist = []
for i in range(0, maxrange, chunksize):
   to_query = cids[i:(i+chunksize)]
   cidlist.extend(get_pubchem(to_query))

dfcid = pd.DataFrame(cidlist)
dfcid['CID'] = dfcid['CID'].astype(str)

df = pd.merge(df, dfcid, left_on='PubChem', right_on='CID', how='left')

df.loc[df['InChI'].isnull(), 'InChI'] = ''

kegg2pubchem['CID'] = kegg2pubchem['CID'].astype(str)
kegg2pubchem = pd.merge(kegg2pubchem, dfcid, on='CID')
kegg_left = df.loc[(df['KEGG']!='') & (df['InChI']==''), 'KEGG']


for k in kegg_left.values:
    if k in kegg2pubchem[1].tolist():
        df.loc[df['KEGG']==k, 'InChI'] = kegg2pubchem.loc[kegg2pubchem[1]==k,
                                                      'InChI'].values[0]

hmdb_left = df.loc[(df['HMDB']!='') & (df['InChI']==''), 'HMDB']

for k in hmdb_left.values:
    try:
        df.loc[df['HMDB']==k, 'InChI'] = hmdb2inchi(k)
    except:
        pass


cas_left = df.loc[(df['CAS']!='') & (df['InChI']==''), 'CAS']
cas_dict = {}

for k in cas_left.values:
    v = cas2cid(k)
    if v is not None:
        cas_dict[k] = v

maxrange = len(cas_dict)
chunksize = 100
cas_values = [str(x) for x in list(cas_dict.values())]
cas2cidlist = []
for i in range(0, maxrange, chunksize):
   to_query = cas_values[i:(i+chunksize)]
   cas2cidlist.extend(get_pubchem(to_query))

cdf = pd.merge(pd.DataFrame(cas2cidlist),
               pd.DataFrame(cas_dict, index=[0]).T.reset_index(),
               left_on='CID', right_on=0)


df = pd.merge(df, cdf[['index', 'InChI', 0]], left_on='CAS', right_on='index', how='left')
df.fillna('', inplace=True)

df.loc[(df['InChI_x']=='') & (df['InChI']!=''), 'InChI_x'] = df.loc[(df['InChI_x']=='') & (df['InChI']!=''), 'InChI']
df.loc[(df['PubChem']=='') & (df[0]!=''), 'PubChem'] = df.loc[(df['PubChem']=='') & (df[0]!=''), 0]
df.drop(['index', 'InChI', 0], axis=1, inplace=True)
