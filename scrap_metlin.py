
#https://metlin.scripps.edu/metabo_info.php?molid=990000
#https://metlin.scripps.edu/showChart.php?molid=1455&h=240&collE=20&Imode=p&etype=experimental
from bs4 import BeautifulSoup
import requests
from multiprocessing import Pool
import sys
import json
import re


def get_entry(molid):
    struct = requests.get('https://metlin.scripps.edu/metabo_info.php?molid=%s' % molid)
    soup = BeautifulSoup(struct.text, "lxml")
    rows = soup.find_all('tr')
    data = {}
    for row in rows:
        ths = row.find_all('th')
        ths = [ele.text.strip() for ele in ths]
        cols = row.find_all('td')
        cols = [ele.text.strip() for ele in cols]
        for ele in zip(ths, cols):
            if ele: # Get rid of empty values
                data[ele[0]] = ele[1]
    return {molid: data}

def get_spectrum(molid, mode='p'):
    # 1455 for test
    spec = requests.get('https://metlin.scripps.edu/showChart.php?molid=%s&h=240&collE=20&Imode=%s&etype=experimental' % (molid, mode))
    soup = BeautifulSoup(spec.text)
    data = soup.find_all('script')[4].string
    #p = re.compile('series: (\[.+\])\n')
    #p.match(data)
    txt = [re.sub('^.+series: ', '', x) for x in data.split('\n') if bool(re.search('series:', x))][0]
    # one observation id 104
    txt = re.sub(', ', '', txt)
    txt = re.sub('&nbsp?;', '', txt)
    txt = re.sub('{(.+?):', '{"\\1":', txt)
    txt = re.sub(',(.+?):', ',"\\1":', txt)
    txt = re.sub('"{', '{', txt)
    txt = re.sub('""', '"', txt)
    txt = re.sub('false', '0', txt)
    txt = re.sub('true', '1', txt)
    txt = re.sub('\'', '"', txt)
    return {molid : json.loads(txt)}

if __name__=='__main__':
    maxrange = int(sys.argv[1])
    chunksize = int(sys.argv[2])
    metlin_dict = {}
    for i in range(1, maxrange, chunksize):
       to_query = range(i, i+chunksize)
       with Pool() as pool:
           result = pool.map(get_entry, to_query)

       [metlin_dict.update(x) for x in result]
       with open('metlin_struct.json', 'w+') as f:
           json.dump(metlin_dict, f)
